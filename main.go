package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"
)

const (
	instrumentationName    = "andyfilya"
	instrumentationVersion = "v0.1"
)

var tracer = otel.GetTracerProvider().Tracer(
	instrumentationName,
	trace.WithInstrumentationVersion(instrumentationVersion),
	trace.WithSchemaURL(semconv.SchemaURL),
)

func wait1(ctx context.Context) {
	_, span := tracer.Start(ctx, "1s")
	time.Sleep(1 * time.Second)
	defer span.End()
}

func wait2(ctx context.Context) {
	_, span := tracer.Start(ctx, "2s")
	time.Sleep(2 * time.Second)
	defer span.End()
}

func Resource() *resource.Resource {
	return resource.NewWithAttributes(
		semconv.SchemaURL,
		semconv.ServiceName("stdout-example"),
		semconv.ServiceVersion("0.0.1"),
	)
}

func InstallExportPipeline(f io.Writer) (func(context.Context) error, error) {
	exporter, err := stdouttrace.New(stdouttrace.WithWriter(f))
	if err != nil {
		return nil, fmt.Errorf("creating stdout exporter: %w", err)
	}

	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(Resource()),
	)
	otel.SetTracerProvider(tracerProvider)

	return tracerProvider.Shutdown, nil
}
func createFile(name string) (io.Writer, error) {
	f, err := os.Create(name)
	if err != nil {
		panic(err)
	}

	return f, nil
}
func main() {
	ctx := context.Background()
	f, _ := createFile("trace.json")
	shutdown, err := InstallExportPipeline(f)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := shutdown(ctx); err != nil {
			panic(err)
		}
	}()

	ctx, span := tracer.Start(ctx, "time sleep functions")
	defer span.End()
	log.Println("traces ended")
}
